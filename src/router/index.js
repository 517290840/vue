import { createRouter, createWebHistory } from "vue-router";
// const App = () => import("../App.vue");
const Home = () => import("../View/Home/home");
const Index = () => import("../View/HomeIndex/index");
const FindHome = () => import("../View/FindHouse/FindHouse.vue");
const Add = () => import("../View/Add/Add.vue");
const ChatList = () => import("../View/Chat_List/Chat_List.vue");
const My = () => import("../View/My/My.vue");
const Login = () => import("../View/Login/Login.vue");
const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
    children: [
      {
        path: "/home/findhome",
        name: "FindHome",
        component: FindHome,
      },
      {
        path: "/home/add",
        name: "Add",
        component: Add,
      },

      {
        path: "/home/index",
        name: "Index",
        component: Index,
      },

      {
        path: "/home/chatlist",
        name: "ChatList",
        component: ChatList,
      },

      {
        path: "/home/my",
        name: "My",
        component: My,
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
router.beforeEach((to, from, next) => {
  if (to.path == "/") next({ name: "Home" });

  next();
});
export default router;
