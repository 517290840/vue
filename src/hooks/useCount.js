import { ref, computed } from "vue";
export const useCount = () => {
  let count = ref(1);
  const changeCount = () => count.value++;
  let result = computed(() => count.value * 2);

  return {
    count,
    changeCount,
    result,
  };
};
