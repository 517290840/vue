import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import "./css/vues.scss";
// px2rem 自适应
import "lib-flexible";
const app = createApp(App).use(router).mount("#app");

export default app;
